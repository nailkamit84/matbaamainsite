import { BrowserRouter, Switch, Route, Routes } from 'react-router-dom';
import './App.css';
import Header from './layout/Header';
import Home from './pages/Home';


function App() {
  return (
    <div className="App">
      <Header/>
      <main className='main'>
        <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<Home />}/>
          </Routes>
        </BrowserRouter>
      </main>
    </div>
  );
}

export default App;
