import React, { useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from "swiper";
import slider1 from '../assets/images/slider1.jpg';
import slider2 from '../assets/images/slider2.png';


// Import Swiper styles
import 'swiper/css';
import "swiper/css/navigation";

function Home(props) {

    return (
        <div >
            <Swiper className='swiper__container' spaceBetween={20} slidesPerView={3} navigation={true} modules={[Navigation]} >
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
                <SwiperSlide className='discover__container'>
                    <img src={slider2} alt="" className="discover__img"/>
                    <div className="discover__data">
                        <h2 className="discover__title">Başlık</h2>
                        <span className="discover__description">Açıklama</span>
                    </div>
                </SwiperSlide>
            </Swiper>
            <div className='home__container'>
                <div className='left__col'>
                    <div className='card__menu'>
                        <div className='leftColumn'>
                        </div>
                        <div className='rightColumn'>
                        </div>

                    </div>
                    <div className='card__menu'>
                        <div className='leftColumn'>
                        </div>
                        <div className='rightColumn'>
                        </div>
                    </div>
                    <div className='card__menu'>
                        <div className='leftColumn'>
                        </div>
                        <div className='rightColumn'>
                        </div>
                    </div>
                </div>
                <div className='right__col'>

                </div>
            </div>

        </div>
    );
}
export default Home;