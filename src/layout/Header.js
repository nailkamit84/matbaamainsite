import React, { useEffect, useState } from 'react';
import { RiShoppingBasketFill } from 'react-icons/ri';
import { GiHamburgerMenu } from 'react-icons/gi';
import { GrClose } from 'react-icons/gr';


function Header(props) {
    const [nav_menu, setNav_menu] = useState(false);
    const [header, setHeader] = useState(false);

    useEffect(() => {
        if (typeof window !== "undefined") {
          window.addEventListener("scroll", () =>
            setHeader(window.pageYOffset >= 80)
          );
        }
      }, []);
      

    return (
        <header className={header ? "header scroll-header": "header"} id="header">
            <nav className="nav container">
                <a href="#" className="nav__logo">LOGO</a>

                <div className="nav__menu" id="nav-menu">
                    <ul className="nav__list">
                        <li className="nav__item">
                            <a href="/" className={window.location.pathname === '/profile' ? "nav__link active-link" : "nav__link"} onClick={()=>{ setNav_menu(false); }}>Giriş Yap</a>
                        </li>
                        <li className="nav__item">
                            <RiShoppingBasketFill fontSize={25}/>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    );
}
export default Header;